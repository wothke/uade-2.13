Short:        EaglePlayer "MaxTrax" external replayer
Author:       Wanted Team
Uploader:     Don Adan <DonAdan@wp.pl>
Version:      2
Type:         mus/play
Replaces:     mus/play/EP_MaxTrax.lha
Architecture: m68k-amigaos

This is a new version of a different external "MaxTrax" replay for
EaglePlayer 1.54/2.00+. It's a player for all known "MaxTrax" modules.

Version 1

Features:

- Analyzer
- Voices
- SampleInfo with SampleSaver (EP 2.00+ only)
- Balance
- ModuleInfo with Position Counter
- Volume
- SongEnd
- SubSong

Version 2

- fixed bug (odd address error for MC68000 Amigas), for some "Contraption Zack"
  songs

Notes:

1. "MaxTrax" is second sound format which is playing scores created or
   edited with Music-X.

2. This player plays tunes in both one-file (with built-in samples)
   and two-file (with external samples) formats.

3. Default file prefixes for two-files format:

   Songdata - MXTX. (for example: MXTX.finalescores)
   Samples  - SMPL. (for example: SMPL.finalescores)

   You can use also "SMPL.set" or "MXTX.main" for the sample file name,
   which is useful for songs that share this same sample file.

4. This player uses the audio device and so wont work on DeliPlayer
   for the PC.

5. All known me MaxTrax's serial numbers:

0001 - Secret Of The Silver Blades
0003 - Pools Of Darkness
0007 - SimEarth v1.0, SimEarth v1.01, SimAnt v1.0
0008 - Might & Magic III
0012 - Dark Queen of Krynn
0015 - Contraption Zack
0017 - Legend Of Kyrandia
0018 - A-Train v1.00
0020 - Dark Seed
0021 - A-Train v1.01, A-Train Construction Set v1.00
0022 - Dune II
0030 - LGPL release

6. You can get the latest versions of Wanted Team products from:

   http://amiga.emucamp.com/wt/wt.html

Special greetings go to

- Joe Pierce for help
- the perfect Gaelan G. for idea
- Henry Cazandra for support
- Jan Krolzig for betatesting
