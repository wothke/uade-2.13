; utility.library offsets extracted from: https://anadoxin.org/blog/amigaos-stdlib-vector-tables.html/
; some of these are used from stonepacker.library (remains to be seen if actually needed..)

FindTagItem					equ -30
GetTagData					equ -36
PackBoolTags				equ -42
NextTagItem					equ -48
FilterTagChanges			equ -54
MapTags						equ -60
AllocateTagItems			equ -66
CloneTagItems				equ -72
FreeTagItems				equ -78
RefreshTagItemClones		equ -84
TagInArray					equ -90
FilterTagItems				equ -96
CallHookPkt					equ -102
Amiga2Date					equ -120
Date2Amiga					equ -126
CheckDate					equ -132
SMult32						equ -138
UMult32						equ -144
SDivMod32					equ -150
UDivMod32					equ -156
Stricmp						equ -162
Strnicmp					equ -168
ToUpper						equ -174
ToLower						equ -180
ApplyTagChanges				equ -186
SMult64						equ -198
UMult64						equ -204
PackStructureTags			equ -210
UnpackStructureTags			equ -216
AddNamedObject				equ -222
AllocNamedObjectA			equ -228
AttemptRemNamedObject		equ -234
FindNamedObject				equ -240
FreeNamedObject				equ -246
NamedObjectName				equ -252
ReleaseNamedObject			equ -258
RemNamedObject				equ -264
GetUniqueID					equ -270
Strlcpy						equ -300
Strlcat						equ -306
VSNPrintf					equ -312
VASPrintf					equ -318
CreateSkipList				equ -324
DeleteSkipList				equ -330
InsertSkipNode				equ -336
FindSkipNode				equ -342
RemoveSkipNode				equ -348
GetFirstSkipNode			equ -354
GetNextSkipNode				equ -360
CreateSplayTree				equ -366
DeleteSplayTree				equ -372
InsertSplayNode				equ -378
FindSplayNode				equ -384
RemoveSplayNode				equ -390
SetMem						equ -396
FindNameNC					equ -402
Random						equ -408
MessageDigest_SHA_Init		equ -414
MessageDigest_SHA_Update	equ -420
MessageDigest_SHA_Final		equ -426